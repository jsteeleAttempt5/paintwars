#pragma once

#include "CoreMinimal.h"
#include "RuntimeMeshActor.h"
#include "CylinderMeshActor.generated.h"


UCLASS()
class PAINTWARS_STEELE_API ACylinderMeshActor : public ARuntimeMeshActor
{
	GENERATED_BODY()

public:
	UMaterialInterface* Material;

public:
	ACylinderMeshActor();

	void OnConstruction(const FTransform& Transform) override;

private:
	virtual void GenerateCylinderMesh();
	virtual void CreateCylinderMesh(FVector BoxRadius,
		TArray<FVector> & Vertices,
		TArray<int32> & Triangles,
		TArray<FVector> & Normals,
		TArray<FVector2D> & UVs,
		TArray<FRuntimeMeshTangent> & Tangents,
		TArray<FColor> & Colors);
};
