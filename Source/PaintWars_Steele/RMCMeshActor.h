#pragma once

#include "CoreMinimal.h"
#include "RuntimeMeshActor.h"
#include "RMCMeshActor.generated.h"


UCLASS()
class PAINTWARS_STEELE_API ARMCMeshActor : public ARuntimeMeshActor
{
	GENERATED_BODY()

public:
	UMaterialInterface* Material;

public:
	ARMCMeshActor();

	void OnConstruction(const FTransform& Transform) override;

private:
	virtual void GenerateBoxMesh();
	virtual void CreateBoxMesh(FVector BoxRadius,
		TArray<FVector> & Vertices,
		TArray<int32> & Triangles,
		TArray<FVector> & Normals,
		TArray<FVector2D> & UVs,
		TArray<FRuntimeMeshTangent> & Tangents,
		TArray<FColor> & Colors);
};
