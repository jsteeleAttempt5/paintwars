// Fill out your copyright notice in the Description page of Project Settings.


#include "RuntimeCubeActor.h"

// Sets default values
ARuntimeCubeActor::ARuntimeCubeActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ARuntimeCubeActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ARuntimeCubeActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

