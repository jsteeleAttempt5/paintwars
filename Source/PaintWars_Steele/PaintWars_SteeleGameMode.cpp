// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "PaintWars_SteeleGameMode.h"
#include "PaintWars_SteeleHUD.h"
#include "PaintWars_SteeleCharacter.h"
#include "UObject/ConstructorHelpers.h"

APaintWars_SteeleGameMode::APaintWars_SteeleGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = APaintWars_SteeleHUD::StaticClass();
}
